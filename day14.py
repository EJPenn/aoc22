with open("day14_input.txt") as file:
    input = file.read()[:-1].split('\n')
    input = [string.split(' -> ') for string in input]

# Specify location of rocks
rock = set()
for line in input:
    for ix in range(len(line)-1):
        start = [int(n) for n in line[ix].split(',')]
        end = [int(n) for n in line[ix+1].split(',')]
        if start[0] == end[0]:
            for y in range(min(start[1], end[1]), max(start[1], end[1])+1):
                rock.add(complex(start[0], y))
        else:
            for x in range(min(start[0], end[0]), max(start[0], end[0])+1):
                rock.add(complex(x, start[1]))


# Function for dropping sand
max_depth = max([int(val.imag) for val in rock])+1

def drop_sand(position = 500 + 0j):
    moving = True
    while moving and (position.imag < max_depth):
        if (position + 1j) not in rock.union(sand):
            position += 1j
        elif (position - 1 + 1j) not in rock.union(sand):
            position += -1 + 1j
        elif (position + 1 + 1j) not in rock.union(sand):
            position += 1 + 1j
        else:
            moving = False

    return(position)

# Part 1
sand = set()
counter = 0
sand_settling = True

while sand_settling:
    new_position = drop_sand()

    # When the sand falls into the void, we stop
    if new_position.imag >= max_depth:
        sand_settling = False
    else:
        sand.add(new_position)
        counter += 1

print(counter)

# Part 2
sand = set()
counter = 0

while new_position != 500:
    new_position = drop_sand()
    sand.add(new_position)
    counter += 1

print(counter)

with open("results.txt", "w") as file:
    # Writing data to a file
    file.write(str(counter))

# Part 2 - smarter attempt!
counter = sum(range(1, 2*(max_depth+1), 2)) - len(rock)