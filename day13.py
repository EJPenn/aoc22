import json


# Functions
def compare_element(x, y):
    if type(x) == int:
        x = [x]
    if type(y) == int:
        y = [y]

    x_contains_list = sum([type(i)==list for i in x])
    y_contains_list = sum([type(i)==list for i in y])

    if x_contains_list or y_contains_list:
        return(compare(x, y))
    elif x < y:
        # 1 means in the right order
        return(1)
    elif x > y:
        # 1 means in the wrong order
        return(-1)
    else:
        # 0 means order undecided
        return(0)


def compare(x, y):
    score = 0
    ix = 0

    while score == 0:
        if ix == len(x):
            score += 1
        elif ix == len(y):
            score += -1
        else:
            score += compare_element(x[ix], y[ix])

        ix += 1

    return(score)


# Load data
with open("day13_input.txt") as file:
    packets = file.read()[:-1].split('\n\n')
    packets = [packet.split('\n') for packet in packets]
    packets = [[json.loads(string) for string in packet] for packet in packets]


# Part 1 - iterate through pairs 
correct_indices = []

for ix, packet_pair in enumerate(packets):
    score = compare(packet_pair[0], packet_pair[1])
    if score == 1:
        correct_indices.append(ix+1)

print(sum(correct_indices))

# Part 2 - sorting 
flat_packets = [item for sublist in packets for item in sublist]
flat_packets.extend([[[2]], [[6]]])

# Let's write a bubble sort algo
def bubble_sort(array):
    n = len(array)

    for i in range(n):
        for j in range(n - i - 1):
            score = compare(array[j], array[j+1])
            if score == -1:
                array[j], array[j+1] = array[j+1], array[j]

    return(array)

sorted_packets = bubble_sort(flat_packets)

print((sorted_packets.index([[2]]) + 1 )*(sorted_packets.index([[6]]) + 1 ))