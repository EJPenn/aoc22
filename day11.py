import math
import re

# Define our Monkey class
class Monkey:
    def __init__(self, id:str, starting_items, operator: str, operation_value: str, divisor: str):

        self.id = int(id)
        self.items = [int(v) for v in starting_items]

        if operation_value == 'old':
            self.operator = '**'
            self.operation_value = 2
        else:
            self.operator = operator
            self.operation_value = int(operation_value)

        self.test_divisor = int(divisor)
        self.false_monkey = None
        self.true_monkey = None

        self.inspection_count = 0

    def operate(self, value: int, method, modulus) -> int:
        if self.operator == '+':
            new_value = value + self.operation_value
        elif self.operator == '*':
            new_value = value * self.operation_value
        elif self.operator == '**':
            new_value = value ** self.operation_value
        else:
            raise ValueError("Unrecognised operator")

        self.inspection_count += 1
        
        if method == "part_1":
            return math.floor(new_value/3)
        elif method == "part_2":
            return(new_value % modulus)
        

    def add_item(self, item: int) -> None:
        self.items.append(item)

    def inspect_items(self, method = "part_1", modulus = None) -> None:
        self.items = [self.operate(item, method, modulus) for item in self.items]

        for item in self.items:
            #print(item)
            if item % self.test_divisor == 0:
                #print("Passes test")
                #print("Passing to monkey", self.true_monkey.id)
                self.true_monkey.add_item(item)
            else:
                #print("Fails test")
                #print("Passing to monkey", self.false_monkey.id)
                self.false_monkey.add_item(item)
            
        self.items = []

# Read in the data and use regex to get the values
with open("day11_input.txt") as file:
    datastream = file.read().split('\n\n')
    for index, monkey in enumerate(datastream):
         instructions = monkey.split('\n')
         datastream[index] = [re.findall(r'\d+|\+|\*|old$', instruction) for instruction in instructions]


def create_monkeys(instructions):
    # Create our monkeys first so we can reference them when setting up monkeys
    monkeys = [
        Monkey(id = instruction_set[0][0],
            starting_items=instruction_set[1],
            operator=instruction_set[2][0],
            operation_value=instruction_set[2][1],
            divisor=instruction_set[3][0]
            ) for instruction_set in datastream]

    # Now set which monkeys items are passed to
    for ix, instruction_set in enumerate(datastream):
        monkeys[ix].true_monkey = monkeys[int(instruction_set[4][0])]
        monkeys[ix].false_monkey = monkeys[int(instruction_set[5][0])]

    return(monkeys)

# Run 20 rounds
monkeys = create_monkeys(datastream)
for i in range(20):
    for monkey in monkeys:
        monkey.inspect_items()

for monkey in monkeys:
    print(monkey.id, monkey.inspection_count)

# Part 2, set up again and run 10000 times
monkeys = create_monkeys(datastream)
modulus = math.prod([monkey.test_divisor for monkey in monkeys])

for i in range(10000):
    for monkey in monkeys:
        monkey.inspect_items(method="part_2", modulus = modulus)

for monkey in monkeys:
    print(monkey.id, monkey.inspection_count)