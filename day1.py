with open("day1_input") as file:
    data = file.read()
    data = data[:-1]
    split_data = [[int(value) for value in elf.split("\n")] for elf in data.split("\n\n")]
    
total_calories = [sum(values) for values in split_data]
total_calories.sort(reverse = True)
print(sum(total_calories[:3]))