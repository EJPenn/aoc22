import pandas as pd

df = pd.read_fwf("day2_input", header = None)

df['shape_score'] = df[1].map({'X': 1, 'Y': 2, 'Z': 3})

score_map = {('A', 'X'): 3,
             ('A', 'Y'): 6,
             ('A', 'Z'): 0,
             ('B', 'X'): 0,
             ('B', 'Y'): 3,
             ('B', 'Z'): 6,
             ('C', 'X'): 6,
             ('C', 'Y'): 0,
             ('C', 'Z'): 3}

df['match_score'] = pd.Series(list(zip(df[0], df[1]))).map(score_map)
print(sum(df['shape_score']) + sum(df['match_score']))

# Part 2
scoring_grid = pd.DataFrame({'X': [3, 1, 2],
                             'Y': [4, 5, 6],
                             'Z': [8, 9, 7]},
                            index = ['A', 'B', 'C'])

df['strategy_score'] = df.apply(lambda x: scoring_grid.loc[(x[0], x[1])], axis=1)
print(sum(df['strategy_score']))