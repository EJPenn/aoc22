from math import prod

with open("day10_input.txt") as file:
    datastream = file.read().split('\n')[:-1]

# History tuples of (cycle, X)
history = [(1,1)]

for ix, val in enumerate(datastream):
    cycle, X = history[-1]
    history.append((cycle+1, X))

    instruction = val.split()
    if instruction[0] == 'addx':
        history.append((cycle+2, X+int(instruction[1])))

# Part 1 solution        
sum([prod(history[19]),
prod(history[59]),
prod(history[99]),
prod(history[139]),
prod(history[179]),
prod(history[219])])

# Part 2
lit_pixels = []
for tick in history:
    """Zero indexing is a pain"""
    cycle, X = tick
    sprite_position = range(X-1, X+2)
    if (cycle-1)%40 in sprite_position:
        lit_pixels.append(cycle-1)

display = ""
for i in range(240):
    if i in lit_pixels:
        display += "#"
    else:
        display += "."

for i in range(6):
    print(display[40*i:40*(i+1)])